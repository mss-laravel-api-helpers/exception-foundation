<?php
/**
 * Test for FormValidationException
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Tests\Unit\Exceptions;

use Gila\LaravelApiHelpers\ExceptionFoundation\Contracts\Exceptions\ApiExceptionContract;
use Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions\FormValidationException;
use Gila\LaravelApiHelpers\ExceptionFoundation\Tests\TestCase;

/**
 * Class FormValidationExceptionTest
 * @package Gila\LaravelApiHelpers\ExceptionFoundation\Tests\Unit\Exceptions
 */
class FormValidationExceptionTest extends TestCase
{
    public function testFormValidationExceptionImplementsApiException()
    {
        $exception = new FormValidationException();
        $this->assertInstanceOf(ApiExceptionContract::class, $exception);
    }

    public function testFormValidationExceptionHasErrorBag()
    {
        $exception = new FormValidationException([
            'first_error' => 'something wrong!',
            'second_error' => 'another problem',
            'nested_errors' => [
                'nested_error1' => 'nested problem',
                'nested_error2' => 'nested problem2'
            ]
        ]);

        $this->assertSame([
            'first_error' => 'something wrong!',
            'second_error' => 'another problem',
            'nested_errors' => [
                'nested_error1' => 'nested problem',
                'nested_error2' => 'nested problem2'
            ]
        ], $exception->getErrorBag());
    }

    public function testFormValidationExceptionCanSetMessage()
    {
        $this->expectException(FormValidationException::class);
        $this->expectExceptionMessage('Custom Message');

        $exception = new FormValidationException([], 'Custom Message');

        throw $exception;
    }
}
