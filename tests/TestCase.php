<?php
/**
 * Base test case
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

/**
 * Class TestCase
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
