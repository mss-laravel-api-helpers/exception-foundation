# REST API Exception Foundation

### About

This package is for use with Laravel 5.4+ REST APIs. The intention of this package is to replace Laravel's default exception renderer
with a renderer that will format exceptions in a more descriptive and strictly-typed way. This package will render all of your API exceptions
as JSON responses.

### Installation

Add the following to your `composer.json`:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/mss-laravel-api-helpers/exception-foundation.git"
    }
]
```

Require via `composer`:
```
composer require gila-api/exception-foundation
```

**Current Version:**
```
composer require gila-api/exception-foundation v1.0.1
```

### Usage

1. In your Laravel API project, open `app/Exceptions/Handler.php`
2. Use the trait: `Gila\LaravelApiHelpers\ExceptionFoundation\Traits\HandlesApiExceptions`
3. Remove the existing `render()` method in the exception handler

Or, import the trait first:
```
use Gila\LaravelApiHelpers\ExceptionFoundation\Traits\HandlesApiExceptions;
```

Then use it in the handler:
```
class Handler extends ExceptionHandler
{
    use HandlesApiExceptions;
    ...
```

**If you require the existing `render()` method for some reason, you may use the trait as follows:**
```
use HandlesApiExceptions {
    HandleApiExceptions::render as renderApiException;
}
```

Then you must call it from your `render()` method:
```
$this->renderApiException($request, $exception);
```

### FormValidationException

```
Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions\FormValidationException
```

Included in this package is a helpful `FormValidationException` that allows you to supply validation errors that will be rendered with the
exception message.

```
throw new FormValidationException(array $validationErrors, string $customMessage, int $httpStatusCode);
```

By default, the message displayed from `FormValidationException` is `The given data was invalid.` You may override this by providing a `$message`
as the second parameter when throwing the exception.

### AbstractApiException

```
Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions\AbstractApiException
```

You may extend this class if you wish to take full advantage of the exception renderer. When extending this class, be sure to implement
the contract `Gila\LaravelApiHelpers\ExceptionFoundation\Contracts\Exceptions\ApiExceptionContract` on the new exception.